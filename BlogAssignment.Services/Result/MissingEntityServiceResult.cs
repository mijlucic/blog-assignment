﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class MissingEntityServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public override int Code { get { return 404; } }

        public MissingEntityServiceResult(String name)
        {
            this._message = String.Format("Dependent entity '{0}' is missing and was not found.", name);
        }
        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitMissingEntity(this);
        }
    }
}