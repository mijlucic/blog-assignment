﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    /// <summary>
    /// Class which represents empty result
    /// </summary>
    public class Nothing
    {
    }
}
