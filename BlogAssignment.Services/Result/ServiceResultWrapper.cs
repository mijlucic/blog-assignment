﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class ServiceResultWrapper<T> : ServiceResult<T>
    {
        public ServiceResult<Nothing> InnerResult { get; private set; }

        public override T Value { get { return default(T); } }

        public override bool IsOk { get { return InnerResult.IsOk; } }

        public override String Message { get { return InnerResult.Message; } }

        public override int Code { get { return InnerResult.Code; } }

        public ServiceResultWrapper(ServiceResult<Nothing> innerResult)
        {
            this.InnerResult = innerResult;
        }
        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return InnerResult.Visit(visitor);
        }
    }
}
