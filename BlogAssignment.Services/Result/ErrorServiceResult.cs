﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class ErrorServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public override int Code { get { return 400; } }

        public ErrorServiceResult(String error, bool prefix = true)
        {
            if (prefix)
            {
                this._message = String.Format("Error: {0}", error);
            }
            else
            {
                this._message = String.Format("{0}", error);
            }
        }

        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitError(this);
        }
    }
}
