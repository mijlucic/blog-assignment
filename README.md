# Documentation #

This readme file represents official documentation of Web.Api project "Blog posts" with Swagger UI API documentation.

###  Technology stack ###
Backend:
*  .NET Core 3.1 SDK
Database: 
* Microsoft SQL Server Express LocalDB
* Model first principle (Entity Framework Core)

###  Technical requirments for build Web API ###
* Visual Studio 2019 IDE
* Microsoft SQL Server Express LocalDB
* .NET Core 3.1 SDK & runtime (IMPORTANT!)

###  How do I get set up? ###
* Open solution in Visual Studio 2019.
* Install dotnet entity framework global tool for execute migrations: 
 > dotnet  tool install --global dotnet-ef
* Execute migrations on database with commands:
> cd ..
> cd .\BlogAssignment.DAL
> dotnet ef database update
* Click on IIS Express build start on Visual Studio 2019.
* You will see Swagger API documentation where you can test all GET, POST, PUT and DELETE requests.
* Enjoy in HTTP requests!
* You can also test all functionality of Web API with Postman tool.