﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace BlogAssignment
{
    public class MvcConfig
    {
        public static void ConfigureServices(
            IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddControllers();

            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
            });

            services.AddCors(options => {
                options.AddPolicy("AllowAny", builder =>
                    builder.AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowAnyOrigin()
                );
            });
        }

        public static void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IConfiguration configuration)
        {
            app.UseRouting();
            app.UseCors("AllowAny");

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

        }
    }
}
