﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class ForbiddenServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public override int Code { get { return 403; } }

        public ForbiddenServiceResult()
        {
            this._message = "Action is forbidden.";
        }

        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitForbiddenError(this);
        }
    }
}
