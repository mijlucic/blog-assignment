﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class NotFoundServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public override int Code { get { return 404; } }

        public NotFoundServiceResult()
            : base("Entity was not found.")
        {
        }
        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitNotFound(this);
        }
    }
}
