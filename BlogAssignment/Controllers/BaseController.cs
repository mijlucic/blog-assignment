﻿using BlogAssignment.Services.Result;
using Microsoft.AspNetCore.Mvc;

namespace BlogAssignment.Controllers
{
    public class BaseController : ControllerBase
    {
        public IActionResult Convert<T>(ServiceResult<T> result)
        {
            var visitor = new ActionResultVisitor<T>();
            return result.Visit(visitor);
        }
    }
}
