﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public abstract class ServiceResult<T>
    {
        public abstract T Value { get; }

        public abstract bool IsOk { get; }

        public abstract int Code { get; }

        public abstract String Message { get; }

        public abstract TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor);

        public static implicit operator ServiceResult<T>(ServiceResult<Nothing> result)
        {
            var wrapperVisitor = new ServiceResultWrapperVisitor<T>();
            return result.Visit(wrapperVisitor);
        }
    }

    public abstract class ServiceResult : ServiceResult<Nothing>
    {
        protected String _message;

        public override Nothing Value { get { return null; } }

        public override String Message { get { return _message; } }

        public ServiceResult()
        {
        }

        public ServiceResult(String message)
        {
            this._message = message;
        }
    }
}
