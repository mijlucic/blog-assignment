﻿using BlogAssignment.MappingModels.Response;
using BlogAssignment.Services.Result;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Interfaces
{
    public interface ITagsService
    {
        ServiceResult<TagListResponseModel> GetTags();
    }
}
