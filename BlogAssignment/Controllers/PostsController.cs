﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogAssignment.MappingModels.Request;
using BlogAssignment.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogAssignment.Controllers
{
    [Route("api/posts/")]
    [ApiController]
    public class PostsController : BaseController
    {
        private IPostsService postsService;
        private readonly ILogger<PostsController> _logger;
        public PostsController(IPostsService _postsService, ILogger<PostsController> logger)
        {
            postsService = _postsService;
            _logger = logger;
        }

        // GET: api/posts/:slug
        [HttpGet("{slug}")]
        public IActionResult GetBlogPost(string slug)
        {
            var result = postsService.GetBlogPost(slug);
            return Convert(result);
        }

        //GET api/posts
        [HttpGet]
        public IActionResult GetListOfBlogPosts([FromQuery] string tag)
        {
            var result = postsService.GetListOfBlogPosts(tag);
            return Convert(result);
        }

        // POST api/posts
        [HttpPost]
        public IActionResult CreateBlogPost([FromBody] BlogPostCreateRequestModel model)
        {
            var result = postsService.CreateBlogPost(model);
            return Convert(result);
        }

        // PUT api/posts/:slug
        [HttpPut("{slug}")]
        public IActionResult UpdateBlogPost(string slug, [FromBody] BlogPostUpdateRequestModel model)
        {
            var result = postsService.UpdateBlogPost(model, slug);
            return Convert(result);
        }

        // DELETE api/posts/:slug
        [HttpDelete("{slug}")]
        public IActionResult DeleteBlogPost(string slug)
        {
            var result = postsService.DeleteBlogPost(slug);
            return Convert(result);
        }
    }
}
