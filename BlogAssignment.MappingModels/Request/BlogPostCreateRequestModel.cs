﻿using BlogAssignment.MappingModels.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.MappingModels.Request
{
    public class BlogPostCreateRequestModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public List<TagListModelItem> TagList { get; set; }
    }
}
