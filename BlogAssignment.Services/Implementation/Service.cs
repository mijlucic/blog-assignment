﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Linq;
using Autofac;
using BlogAssignment.Services.Result;

namespace BlogAssignment.Services.Implementation
{
    public class Service
    {
        //public ILifetimeScope Scope { get; set; }

        public Service()
        {
            //this.Scope = scope;
        }

        public OkServiceResult Ok()
        {
            return new OkServiceResult();
        }

        public OkServiceResult<T> Ok<T>(T value)
        {
            return new OkServiceResult<T>(value);
        }

        public NotFoundServiceResult NotFound()
        {
            return new NotFoundServiceResult();
        }

        public ErrorServiceResult Error(String message)
        {
            return new ErrorServiceResult(message);
        }

        public ErrorServiceResult Error(String message, params String[] args)
        {
            message = String.Format(message, args);
            return new ErrorServiceResult(message);
        }

        public ValidationErrorServiceResult ValidationError(String message)
        {
            return new ValidationErrorServiceResult(message);
        }

        public ValidationErrorServiceResult ValidationError(String message, params String[] args)
        {
            message = String.Format(message, args);
            return new ValidationErrorServiceResult(message);
        }

        public MissingEntityServiceResult MissingEntity(String name)
        {
            return new MissingEntityServiceResult(name);
        }
        
    }
}