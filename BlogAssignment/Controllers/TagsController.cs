﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogAssignment.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogAssignment.Controllers
{
    [Route("api/tags/")]
    [ApiController]
    public class TagsController : BaseController
    {
        private ITagsService tagService;
        private readonly ILogger<TagsController> _logger;
        public TagsController(ITagsService _tagService, ILogger<TagsController> logger)
        {
            tagService = _tagService;
            _logger = logger;
        }

        // GET: api/tags
        [HttpGet]
        public IActionResult GetTags()
        {
            var result = tagService.GetTags();
            return Convert(result);
        }
    }
}
