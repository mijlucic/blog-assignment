﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public interface IServiceResultVisitor<Tin, TRet>
    {
        TRet VisitOk(OkServiceResult<Tin> result);

        TRet VisitNotFound(NotFoundServiceResult result);

        TRet VisitForbiddenError(ForbiddenServiceResult forbiddenServiceResult);

        TRet VisitError(ErrorServiceResult errorResult);

        TRet VisitValidationError(ValidationErrorServiceResult validationErrorResult);

        TRet VisitMissingEntity(MissingEntityServiceResult missingEntityResult);

        TRet VisitExistingEntity(ExistingEntityServiceResult existingEntityResult);
    }
}
