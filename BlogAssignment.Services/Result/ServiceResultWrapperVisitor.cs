﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class ServiceResultWrapperVisitor<TIn> : IServiceResultVisitor<TIn, ServiceResultWrapper<TIn>>
    {
        public ServiceResultWrapper<TIn> VisitOk(OkServiceResult<TIn> result)
        {
            throw new InvalidOperationException();
        }

        public ServiceResultWrapper<TIn> VisitNotFound(NotFoundServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitError(ErrorServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitValidationError(ValidationErrorServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitMissingEntity(MissingEntityServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitExistingEntity(ExistingEntityServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }

        public ServiceResultWrapper<TIn> VisitForbiddenError(ForbiddenServiceResult result)
        {
            return new ServiceResultWrapper<TIn>(result);
        }
    }
}
