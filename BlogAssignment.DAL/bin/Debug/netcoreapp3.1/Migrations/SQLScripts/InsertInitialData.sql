USE [BlogAssignment]
GO

INSERT INTO [dbo].[BlogPost]
           ([CreatedAt]
           ,[UpdatedAt]
           ,[Slug]
           ,[Title]
           ,[Description]
           ,[Body])
     VALUES
           (GETDATE(),
		   GETDATE(),
		   N'augmented-reality-ios-application',
		   N'Augmented Reality iOS Application',
		   N'Rubicon Software Development and Gazzda furniture are proud to launch an augmented reality app.',
		   N'The app is simple to use, and will help you decide on your best furniture fit.'
		   )

INSERT INTO [dbo].[BlogPost]
           ([CreatedAt]
           ,[UpdatedAt]
           ,[Slug]
           ,[Title]
           ,[Description]
           ,[Body])
     VALUES
           (GETDATE(),
		   GETDATE(),
		   N'augmented-reality-ios-application-2',
		   N'Augmented Reality iOS Application 2',
		   N'Rubicon Software Development and Gazzda furniture are proud to launch an augmented reality app.',
		   N'The app is simple to use, and will help you decide on your best furniture fit.'
		   )

INSERT INTO [dbo].[Tag]
           ([CreatedAt]
           ,[UpdatedAt]
           ,[Name])
     VALUES
           (GETDATE(),
		   GETDATE(),
		   N'iOS'
		   )

INSERT INTO [dbo].[Tag]
           ([CreatedAt]
           ,[UpdatedAt]
           ,[Name])
     VALUES
           (GETDATE(),
		   GETDATE(),
		   N'AR'
		   )

INSERT INTO [dbo].[Tag]
           ([CreatedAt]
           ,[UpdatedAt]
           ,[Name])
     VALUES
           (GETDATE(),
		   GETDATE(),
		   N'Gazzda'
		   )

INSERT INTO [dbo].[BlogPostTag]
           ([BlogPostId]
           ,[TagId])
     VALUES
           (
		   (select top(1) Id from BlogPost where Slug='augmented-reality-ios-application'),
		   (select top(1) Id from Tag where Name='iOS')
		   )
INSERT INTO [dbo].[BlogPostTag]
           ([BlogPostId]
           ,[TagId])
     VALUES
           (
		   (select top(1) Id from BlogPost where Slug='augmented-reality-ios-application'),
		   (select top(1) Id from Tag where Name='AR')
		   )

INSERT INTO [dbo].[BlogPostTag]
           ([BlogPostId]
           ,[TagId])
     VALUES
           (
		   (select top(1) Id from BlogPost where Slug='augmented-reality-ios-application-2'),
		   (select top(1) Id from Tag where Name='iOS')
		   )
INSERT INTO [dbo].[BlogPostTag]
           ([BlogPostId]
           ,[TagId])
     VALUES
           (
		   (select top(1) Id from BlogPost where Slug='augmented-reality-ios-application-2'),
		   (select top(1) Id from Tag where Name='AR')
		   )
INSERT INTO [dbo].[BlogPostTag]
           ([BlogPostId]
           ,[TagId])
     VALUES
           (
		   (select top(1) Id from BlogPost where Slug='augmented-reality-ios-application-2'),
		   (select top(1) Id from Tag where Name='Gazzda')
		   )
GO
