﻿using BlogAssignment.MappingModels.Request;
using BlogAssignment.MappingModels.Response;
using BlogAssignment.Services.Result;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Interfaces
{
    public interface IPostsService
    {
        ServiceResult<BlogPostResponseModel> GetBlogPost(string slug);
        ServiceResult<BlogPostListResponseModel> GetListOfBlogPosts(string tag);
        ServiceResult<BlogPostResponseModel> CreateBlogPost(BlogPostCreateRequestModel model);
        ServiceResult<Nothing> UpdateBlogPost(BlogPostUpdateRequestModel model, string slug);
        ServiceResult<Nothing> DeleteBlogPost(string slug);
    }
}
