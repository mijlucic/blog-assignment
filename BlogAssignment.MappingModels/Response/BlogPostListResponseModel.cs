﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.MappingModels.Response
{
    public class BlogPostListResponseModel
    {
        public List<BlogPostResponseModel> BlogPosts { get; set; }
    }
}
