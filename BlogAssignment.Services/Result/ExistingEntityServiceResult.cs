﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class ExistingEntityServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public override int Code { get { return 400; } }

        public ExistingEntityServiceResult(String propetyName)
        {
            this._message = String.Format("Entity already exists, conflict in property '{0}'.", propetyName);
        }
        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitExistingEntity(this);
        }
    }
}
