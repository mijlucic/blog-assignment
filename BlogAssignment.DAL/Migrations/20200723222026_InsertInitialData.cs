﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace BlogAssignment.DAL.Migrations
{
    public partial class InsertInitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var sqlFile = Path.Combine(baseDirectory, "Migrations/SQLScripts", "InsertInitialData.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
