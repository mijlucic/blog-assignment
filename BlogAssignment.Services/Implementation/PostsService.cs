﻿using Autofac;
using BlogAssignment.DAL;
using BlogAssignment.DAL.Models;
using BlogAssignment.MappingModels.Request;
using BlogAssignment.MappingModels.Response;
using BlogAssignment.Services.Interfaces;
using BlogAssignment.Services.Result;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Schema;

namespace BlogAssignment.Services.Implementation
{
    public class PostsService : Service, IPostsService
    {
        private ApplicationDbContext context;

        public PostsService(ApplicationDbContext _context) 
        {
            context = _context;
        }

        public ServiceResult<BlogPostResponseModel> GetBlogPost(string slug)
        {
            BlogPostResponseModel responseModel;

            if (String.IsNullOrEmpty(slug))
            {
                return Error("Slug parameter is null or empty. Please, try again.");
            }

            var blogPost = context.BlogPost
                        .Include(x => x.BlogPostTags)
                        .Where(x => x.Slug == slug)
                        .FirstOrDefault();

            if(blogPost == null)
            {
                return NotFound();
            }

            responseModel = new BlogPostResponseModel
            {
                Slug = blogPost.Slug,
                Title = blogPost.Title,
                Body = blogPost.Body,
                Description = blogPost.Description,
                CreatedAt = blogPost.CreatedAt,
                UpdatedAt = blogPost.UpdatedAt,
                TagList = blogPost.BlogPostTags.Select(x => new TagListModelItem
                {
                    Tag = x.Tag != null ? x.Tag.Name : ""
                }).ToList()
            };

            return Ok(responseModel);
        }

        public ServiceResult<BlogPostListResponseModel> GetListOfBlogPosts(string tag)
        {
            BlogPostListResponseModel responseModel;

            var listOfBlogPosts = context.BlogPost
                        .Include(x => x.BlogPostTags)
                        .AsQueryable();

            if (!String.IsNullOrEmpty(tag))
            {
                var blogTagsList = context.BlogPostTag
                                .Include(x => x.Tag)
                                .Where(x => x.Tag.Name.Contains(tag))
                                .AsQueryable();
                listOfBlogPosts = listOfBlogPosts
                    .Where(x => x.BlogPostTags.Any(y => blogTagsList.Any(z => z.Id == y.Id)))
                    .AsQueryable();
            }

            responseModel = new BlogPostListResponseModel
            {
                BlogPosts = listOfBlogPosts.Select(x => new BlogPostResponseModel
                {
                    Slug = x.Slug,
                    Body = x.Body,
                    Description = x.Description,
                    Title = x.Title,
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt,
                    TagList = x.BlogPostTags.Select(t => new TagListModelItem
                    {
                        Tag = t.Tag.Name
                    }).ToList()
                }).ToList()
            };
            return Ok(responseModel);
        }

        public ServiceResult<BlogPostResponseModel> CreateBlogPost(BlogPostCreateRequestModel model)
        {
            BlogPost blogPost;
            try
            {
                blogPost = new BlogPost
                {
                    Slug = CreateSlugFromTitle(model.Title),
                    Title = model.Title,
                    Body = model.Body,
                    Description = model.Description,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                };
                context.BlogPost.Add(blogPost);
                context.SaveChanges();

                var existTag = context.Tag.Where(x => model.TagList.Any(y => y.Tag == x.Name)).FirstOrDefault();
                if(existTag == null)
                {
                    foreach (var tag in model.TagList)
                    {
                        var newTag = new Tag
                        {
                            Name = tag.Tag,
                            CreatedAt = DateTime.Now,
                            UpdatedAt = DateTime.Now,
                        };
                        context.Tag.Add(newTag);

                        var newBlogTag = new BlogPostTag
                        {
                            BlogPostId = blogPost.Id,
                            TagId = newTag.Id,
                        };
                        context.BlogPostTag.Add(newBlogTag);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                return Error("Can't create new blog post. Please, try again.");
            }

            BlogPostResponseModel responseModel = new BlogPostResponseModel
            {
                Slug = blogPost.Slug,
                Title = blogPost.Title,
                Body = blogPost.Body,
                Description = blogPost.Description,
                CreatedAt = blogPost.CreatedAt,
                UpdatedAt = blogPost.UpdatedAt,
                TagList = blogPost.BlogPostTags.Select(t => new TagListModelItem
                {
                    Tag = t.Tag.Name
                }).ToList()
            };
            return Ok(responseModel);
        }

        public string CreateSlugFromTitle(string title)
        {
            byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(title);
            string str = Encoding.ASCII.GetString(bytes);
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        public ServiceResult<Nothing> UpdateBlogPost(BlogPostUpdateRequestModel model, string slug)
        {
            var existingBlogPost = context.BlogPost.Where(x => x.Slug == slug).FirstOrDefault();

            #region Backend validation
            if (String.IsNullOrEmpty(model.Title))
            {
                return Error("Title is empty! Please, fill that field.");
            }
            #endregion

            try
            {
                existingBlogPost.Title = model.Title;
                existingBlogPost.UpdatedAt = DateTime.Now;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return Error("Update failed.");
            }

            return Ok();
        }

        public ServiceResult<Nothing> DeleteBlogPost(string slug)
        {
            try
            {
                var existingBlogPost = context.BlogPost.Where(x => x.Slug == slug).FirstOrDefault();

                context.BlogPost.Remove(existingBlogPost);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return Error("Delete blog post error.");
            }
            return Ok();
        }
    }
}
