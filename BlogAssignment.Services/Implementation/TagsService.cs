﻿using Autofac;
using BlogAssignment.DAL;
using BlogAssignment.MappingModels.Response;
using BlogAssignment.Services.Interfaces;
using BlogAssignment.Services.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlogAssignment.Services.Implementation
{
    public class TagsService : Service, ITagsService
    {
        private ApplicationDbContext context;

        public TagsService(ApplicationDbContext _context)
        {
            context = _context;
        }

        public ServiceResult<TagListResponseModel> GetTags()
        {
            TagListResponseModel responseModel;

            var tags = context.Tag.ToList();

            responseModel = new TagListResponseModel
            {
                Tags = tags.Select(x => new TagListModelItem
                {
                    Tag = x.Name
                }).ToList()
            };

            return Ok(responseModel);
        }
    }
}
