﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.Services.Result
{
    public class ValidationErrorServiceResult : ServiceResult
    {
        public override bool IsOk { get { return false; } }

        public override int Code { get { return 400; } }

        public ValidationErrorServiceResult(String error)
        {
            this._message = String.Format("Validation error: {0}", error);
        }
        public override TRet Visit<TIn, TRet>(IServiceResultVisitor<TIn, TRet> visitor)
        {
            return visitor.VisitValidationError(this);
        }
    }
}
