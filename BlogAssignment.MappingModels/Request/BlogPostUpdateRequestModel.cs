﻿using BlogAssignment.MappingModels.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.MappingModels.Request
{
    public class BlogPostUpdateRequestModel
    {
        public string Title { get; set; }
    }
}
