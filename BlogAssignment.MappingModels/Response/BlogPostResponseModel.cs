﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.MappingModels.Response
{
    public class BlogPostResponseModel
    {
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public List<TagListModelItem> TagList { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
