﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogAssignment.MappingModels.Response
{
    public class TagListResponseModel
    {
        public List<TagListModelItem> Tags { get; set; }
    }
}
