﻿namespace BlogAssignment.MappingModels.Response
{
    public class TagListModelItem
    {
        public string Tag { get; set; }
    }
}