﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using BlogAssignment.Services.Result;

namespace BlogAssignment
{
    public class ActionResultVisitor<T> : IServiceResultVisitor<T, IActionResult>
    {
        public IActionResult VisitOk(OkServiceResult<T> result)
        {
            return new OkObjectResult(result.Value);
        }

        public IActionResult VisitNotFound(NotFoundServiceResult result)
        {
            return new NotFoundResult();
        }

        public IActionResult VisitError(ErrorServiceResult errorResult)
        {
            return new BadRequestObjectResult(errorResult.Message);
        }

        public IActionResult VisitValidationError(ValidationErrorServiceResult validationErrorResult)
        {
            return new BadRequestObjectResult(validationErrorResult.Message);
        }

        public IActionResult VisitMissingEntity(MissingEntityServiceResult missingEntityResult)
        {
            return new NotFoundObjectResult(missingEntityResult.Message);
        }

        public IActionResult VisitExistingEntity(ExistingEntityServiceResult existingEntityResult)
        {
            var result = new ObjectResult(existingEntityResult.Message);
            result.StatusCode = StatusCodes.Status409Conflict;
            return result;
        }

        public IActionResult VisitForbiddenError(ForbiddenServiceResult forbiddenServiceResult)
        {
            var result = new ObjectResult("Nemate prava na ovu akciju.");
            result.StatusCode = StatusCodes.Status403Forbidden;
            return result;
        }
    }
}
